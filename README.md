Ansible
=======

[![pipeline status](https://gitlab.com/yoanncolin/container-images/ansible/badges/main/pipeline.svg)](https://gitlab.com/yoanncolin/container-images/ansible/-/commits/main)

Container image with [Ansible][] and CLI for Docker, Libvirt, Podman and OpenStack.

GitLab Project : [yoanncolin/container-images/ansible][].

[Ansible]: https://github.com/ansible
[yoanncolin/container-images/ansible]: https://gitlab.com/yoanncolin/container-images/ansible

Tags
----

Check [end of life dates][] before chosing your version :

* [`2.18.1`][latest], [`2.18`][latest], [`2`][latest], [`2.18.1-alpine`][latest], [`2.18-alpine`][latest], [`2-alpine`][latest], [`alpine`][latest], [`latest`][latest] (Weekly build)
* [`2.18.1-debian`][debian], [`2.18-debian`][debian], [`2-debian`][debian], [`debian`][debian] (Weekly build)
* [`2.17.0`][2.17], [`2.17.0-alpine`][2.17] (Monthly build)
* [`2.16.1`][2.16], [`2.16.1-alpine`][2.16], [`2.16-alpine`][2.16] (Monthly build)
* [`2.14.5`][2.14], [`2.14`][2.14], [`2.14.5-alpine`][2.14], [`2.14-alpine`][2.14] (Deprecated)
* [`2.13.6`][2.13], [`2.13`][2.13], [`2.13.6-alpine`][2.13], [`2.13-alpine`][2.13] (Deprecated)
* [`2.11.6`][2.11], [`2.11`][2.11], [`2.11.6-alpine`][2.11], [`2.11-alpine`][2.11] (Deprecated)
* [`2.10.5`][2.10], [`2.10`][2.10], [`2.10.5-alpine`][2.10], [`2.10-alpine`][2.10] (Deprecated)

[end of life dates]: https://endoflife.date/ansible-core
[debian]: https://gitlab.com/yoanncolin/container-images/ansible/-/blob/debian/Dockerfile
[latest]: https://gitlab.com/yoanncolin/container-images/ansible/-/blob/main/Dockerfile
[2.17]: https://gitlab.com/yoanncolin/container-images/ansible/-/blob/2.17/Dockerfile
[2.16]: https://gitlab.com/yoanncolin/container-images/ansible/-/blob/2.16/Dockerfile
[2.14]: https://gitlab.com/yoanncolin/container-images/ansible/-/blob/2.14/Dockerfile
[2.13]: https://gitlab.com/yoanncolin/container-images/ansible/-/blob/2.13/Dockerfile
[2.11]: https://gitlab.com/yoanncolin/container-images/ansible/-/blob/2.11/Dockerfile
[2.10]: https://gitlab.com/yoanncolin/container-images/ansible/-/blob/2.10/Dockerfile

Software versions
-----------------

|         Tag         |     From      |   Docker   | Podman  | Libvirt  | Openstack-CLI |  Python   |  OpenJDK  |
| :-----------------: | :-----------: | :--------: | :-----: | :------: | :-----------: | :-------: | :-------: |
|    **`2.18.1`**     | `alpine:3.21` |  `27.3.1`  | `5.3.2` | `10.9.0` |   Dalmatian   | `3.12.9`  | `21.0.6`  |
| **`2.18.1-debian`** | `debian:sid`  |  `27.5.1`  | `5.3.2` | `11.0.0` |   Dalmatian   | `3.13.2`  | `23.0.2`  |
|    **`2.17.0`**     | `alpine:3.20` |  `26.1.5`  | `5.2.5` | `10.7.0` |    Caracal    | `3.12.8`  | `21.0.5`  |
|    **`2.16.1`**     | `alpine:3.19` |  `25.0.5`  | `4.8.3` | `9.10.0` |    Bobcat     | `3.11.11` | `21.0.5`  |
|    **`2.14.5`**     | `alpine:3.18` |  `25.0.3`  | `4.5.1` | `9.3.0`  |     Yoga      | `3.11.8`  | `17.0.10` |
|    **`2.13.6`**     | `alpine:3.17` | `20.10.24` | `4.3.1` | `8.9.0`  |     Yoga      | `3.10.13` | `17.0.10` |
|    **`2.11.6`**     | `alpine:3.15` | `20.10.16` | `3.4.7` | `7.9.0`  |     Xena      | `3.9.18`  | `17.0.8`  |
|    **`2.10.5`**     | `alpine:3.14` | `20.10.11` | `3.2.3` | `7.4.0`  |    Wallaby    | `3.9.17`  | `11.0.14` |

Usage
-----

Simple usages

```sh
docker run gwerlas/ansible ansible --version
docker run gwerlas/ansible ansible-galaxy collection list
docker run -v <my-dir>:/work -w /work ansible-playbook <my-play>.yml
```

Libvirt in Docker :

```sh
docker run \
  --privileged \
  -v /run/libvirt:/run/libvirt \
  gwerlas/ansible ...
```

Docker / Podman in Docker :

```sh
docker run \
  --privileged \
  -v /run/docker.sock:/run/docker.sock \
  -v /sys/fs/cgroup:/sys/fs/cgroup \
  gwerlas/ansible ...
```

Docker in Podman rootless :

```
systemctl start --user podman.service
podman run \
  --privileged \
  -v $XDG_RUNTIME_DIR/podman/podman.sock:/run/docker.sock \
  -v /sys/fs/cgroup:/sys/fs/cgroup \
  gwerlas/ansible ...
```

Podman use the socket of the host :

```
systemctl start --user podman.service
podman run \
  --privileged \
  -v $XDG_RUNTIME_DIR/podman/podman.sock:/run/docker.sock \
  -v /sys/fs/cgroup:/sys/fs/cgroup \
  -e CONTAINER_CONNECTION=docker \
  gwerlas/ansible ...
```

### Gitlab-CI

```yaml
my-job:
  image: gwerlas/ansible
  script:
    - ansible-galaxy install -r requirements.txt
    - ansible-playbook playbooks/site.yml
```

With Docker :

```yaml
my-job:
  image: gwerlas/ansible
  services:
    - docker:27.4-dind
  script:
    - ansible-galaxy install -r requirements.txt
    - ansible-playbook playbooks/site.yml
```

Builts in
---------

### System tools

- buildah
- docker
- git
- goss
- kubectl (Since 2.16)
- openjdk
- openstack
- openssl
- podman
- rsync
- ssh
- sshpass
- virt-install
- yq

### Python modules

- cryptography
- jmespath
- netaddr
- netifaces
- psycopg2 (PostgreSQL client)
- xmltodict

### Ansible modules

- yay (ArchLinux User Repository)

### Collections

- amazon.aws
- ansible.netcommon
- ansible.posix
- ansible.utils (Since `2.11`)
- ansible.windows
- arista.eos
- awx.awx
- azure.azcollection
- check_point.mgmt
- chocolatey.chocolatey
- cisco.aci
- cisco.asa
- cisco.dnac (Since `2.13.6`)
- cisco.intersight
- cisco.ios
- cisco.iosxr
- cisco.ise (Since `2.13`)
- cisco.meraki
- cisco.mso
- cisco.nxos
- cisco.ucs
- cloud.common (Since `2.13`)
- cloudscale_ch.cloud
- community.aws
- community.ciscosmb (Since `2.13`)
- community.crypto
- community.digitalocean
- community.dns (Since `2.13`)
- community.docker
- community.general
- community.grafana
- community.hashi_vault
- community.hrobot
- community.library_inventory_filtering_v1 (Since `2.17`)
- community.libvirt
- community.mongodb
- community.mysql
- community.network
- community.okd
- community.postgresql
- community.proxysql
- community.rabbitmq
- community.routeros
- community.sap_libs (Since `2.13.6`)
- community.sops (Since `2.11`)
- community.vmware
- community.windows
- community.zabbix
- containers.podman
- cyberark.conjur
- cyberark.pas
- dellemc.enterprise_sonic (Since `2.11`)
- dellemc.openmanage (Since `2.11`)
- dellemc.powerflex (Since `2.14`)
- dellemc.unity (Since `2.14`)
- f5networks.f5_modules
- fortinet.fortimanager
- fortinet.fortios
- google.cloud
- grafana.grafana (Since `2.15`)
- hetzner.hcloud
- ibm.qradar
- ibm.spectrum_virtualize (Since `2.15`)
- ibm.storage_virtualize (Since `2.16`)
- ieisystem.inmanage (Since `2.18`)
- infinidat.infinibox
- infoblox.nios_modules (Since `2.13`)
- inspur.ispim (Since `2.13.6`)
- junipernetworks.junos
- kaytus.ksmanage (Since `2.18`)
- kubernetes.core (Since `2.11`)
- kubevirt.core (Since `2.18`)
- lowlydba.sqlserver (Since `2.13.6`)
- microsoft.ad (Since `2.14`)
- netapp.cloudmanager (Since `2.11`)
- netapp.ontap
- netapp.storagegrid (Since `2.13`)
- netapp_eseries.santricity
- netbox.netbox
- ngine_io.cloudstack
- openstack.cloud
- ovirt.ovirt
- purestorage.flasharray
- purestorage.flashblade
- sensu.sensu_go (Since `2.11`)
- splunk.es
- telekom_mms.icinga_director (Since `2.16`)
- theforeman.foreman
- vmware.vmware_rest (Since `2.13`)
- vultr.cloud (Since `2.14`)
- vyos.vyos
- wti.remote

### OpenStack clients

* [barbican](https://wiki.openstack.org/wiki/barbican)
* [cinder](https://wiki.openstack.org/wiki/cinder)
* [designate](https://wiki.openstack.org/wiki/designate)
* [glance](https://wiki.openstack.org/wiki/glance)
* [heat](https://wiki.openstack.org/wiki/heat)
* [ironic](https://wiki.openstack.org/wiki/ironic)
* [keystone](https://wiki.openstack.org/wiki/keystone)
* [magnum](https://wiki.openstack.org/wiki/magnum)
* [mistral](https://docs.openstack.org/mistral)
* [neutron](https://docs.openstack.org/neutron/)
* [nova](https://wiki.openstack.org/wiki/nova)
* [octavia](https://wiki.openstack.org/wiki/octavia)
* [swift](https://wiki.openstack.org/wiki/swift)
* [tacker](https://wiki.openstack.org/wiki/tacker)
* [trove](https://wiki.openstack.org/wiki/trove)
* [vitrage](https://wiki.openstack.org/wiki/vitrage)
* [zaqar](https://wiki.openstack.org/wiki/zaqar)
* [zun](https://wiki.openstack.org/wiki/zun)

License
-------

[BSD 3-Clause License](LICENSE).
