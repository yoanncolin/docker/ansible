# Inspired from https://www.redhat.com/sysadmin/podman-inside-container

# There is a package version conflicts between ansible-core:2.18.2 and
# resolvelib:1.1.0 in edge breaking ansible-galaxy
FROM alpine:3.21

# Tools
ADD https://github.com/aelsabbahy/goss/releases/latest/download/goss-linux-amd64 /bin/goss
RUN chmod +x /bin/goss && \
    apk upgrade --no-cache && \
    apk add --no-cache \
        ca-certificates git kubectl openjdk21-jre-headless openssh-client openssl \
        python3 py3-pip rsync sshpass tar yq zip && \
    git config --global --add safe.directory /work

# Ansible himself
RUN apk add --no-cache \
        py3-cryptography py3-jmespath py3-jinja2 py3-netaddr py3-netifaces \
        py3-psycopg2 py3-xmltodict py3-yaml \
        ansible

# Yay module
ADD https://raw.githubusercontent.com/mnussbaum/ansible-yay/master/yay /usr/share/ansible/yay

# Libvirt support
RUN apk add --no-cache libosinfo libvirt-client py3-libvirt py3-lxml virt-install

# Docker support
RUN apk add --no-cache docker-cli docker-cli-buildx py3-docker-py

RUN mkdir -p /certs/client && chmod 1777 /certs /certs/client

ENV DOCKER_TLS_CERTDIR=/certs

# Openstack support
COPY files/requirements.txt /tmp/requirements.txt
RUN apk add --no-cache \
        py3-attrs py3-autopage py3-babel py3-cliff py3-dateutil py3-decorator \
        py3-iso8601 py3-jsonpatch py3-jsonpointer py3-jsonschema py3-msgpack \
        py3-networkx py3-openssl py3-pbr py3-prettytable py3-psutil py3-pydot \
        py3-pyperclip py3-requests py3-rfc3986 py3-simplejson py3-stevedore \
        py3-tzdata py3-urllib3 py3-wcwidth py3-wrapt && \
    pip install \
        --break-system-packages \
        --no-cache-dir \
        --root-user-action ignore \
        -r /tmp/requirements.txt

# Podman support
RUN apk add --no-cache buildah fuse-overlayfs podman

COPY files/storage.conf /etc/containers/storage.conf
COPY files/containers.conf /etc/containers/containers.conf
COPY files/libpod.conf /etc/containers/libpod.conf

RUN chmod 644 /etc/containers/*.conf && \
    mkdir -p \
        /var/lib/shared/overlay-images \
        /var/lib/shared/overlay-layers \
        /var/lib/shared/vfs-images \
        /var/lib/shared/vfs-layers && \
    touch \
        /var/lib/shared/overlay-images/images.lock \
        /var/lib/shared/overlay-layers/layers.lock \
        /var/lib/shared/vfs-images/images.lock \
        /var/lib/shared/vfs-layers/layers.lock

VOLUME /var/lib/containers

ENV _CONTAINERS_USERNS_CONFIGURED=""
ENV _BUILDAH_STARTED_IN_USERNS="" BUILDAH_ISOLATION=chroot

# Entrypoint
COPY files/entrypoint /usr/local/sbin/

ENTRYPOINT ["/usr/local/sbin/entrypoint"]
CMD ["sh"]
