Ansible
=======

Dependencies
------------

Install and configure and start Libvirt and
a container engine (Docker or Podman).

Test'n dev
----------

Using Docker :

```sh
systemctl start docker.socket
docker buildx build --pull --iidfile /tmp/iid .
docker run --rm -t \
    --privileged --security-opt label=disable \
    -v /run/libvirt:/run/libvirt \
    -v /run/docker.sock:/run/docker.sock \
    -v /sys/fs/cgroup:/sys/fs/cgroup:ro \
    -v $PWD:$PWD -w $PWD \
    $(cat /tmp/iid) \
    ansible-playbook verify.yml
```

Using Podman :

```sh
systemctl start --user podman.socket
podman build --pull=newer --iidfile /tmp/iid .
podman run --rm -t \
    --privileged --security-opt label=disable \
    -v /run/libvirt:/run/libvirt \
    -v $XDG_RUNTIME_DIR/podman/podman.sock:/run/docker.sock \
    -v /sys/fs/cgroup:/sys/fs/cgroup:ro \
    -v $PWD:$PWD -w $PWD \
    $(cat /tmp/iid) \
    ansible-playbook verify.yml
```

Test Galaxy :

```sh
podman run --rm -t \
    --security-opt label=disable \
    -v $PWD:$PWD -w $PWD \
    $(cat /tmp/iid) \
    ansible-galaxy collection install -r requirements.yml
```

Show software versions to update the README :

```sh
podman run --rm -t \
    --security-opt label=disable \
    -v $PWD:$PWD -w $PWD \
    $(cat /tmp/iid) \
    ansible-playbook info.yml
```

